//
//  MSPainting.h
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSPainting : NSObject
#pragma mark designated initializer
- (instancetype)initWithData:(id)data;

#pragma mark Painting Properties
@property (strong, nonatomic, getter=getImageURL)         NSURL     *image_url;
@property (strong, nonatomic, getter=getInfoText)         NSString  *info_text;
@property (nonatomic, getter=getInfoTextSize)             NSInteger info_text_size;
@property (strong, nonatomic, getter=getInfoTextColor)    NSString  *info_text_color;
@property (assign, nonatomic, getter=isTextViewClicked)   BOOL      textViewClicked;

@end
