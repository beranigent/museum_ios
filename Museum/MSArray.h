//
//  NSMutableArray+MSArray.h
//  Museum
//
//  Created by Gent Berani on 7/23/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (MSArray)

#pragma mark designated initializer
-(instancetype)initPaintingArrayWithData:(id)data;

@end
