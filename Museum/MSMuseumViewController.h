//
//  MSMuseumViewController.h
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSMuseumViewCell.h"
@interface MSMuseumViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,MuseumCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *noResultView;

#pragma mark Painting Array that consists all Painting Objects
@property (nonatomic, strong) NSMutableArray* paintingArray;
- (void)requestForPaintingData;
@end

