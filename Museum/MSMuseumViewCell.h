//
//  MSMuseumViewCell.h
//  Museum
//
//  Created by Gent Berani on 7/23/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSPainting.h"

#pragma mark TextView Click Protocol
@protocol MuseumCellDelegate <NSObject>
- (void)didUserClickOnTextViewAtIndex:(NSInteger)cellIndex;
@end

@interface MSMuseumViewCell : UICollectionViewCell

@property (weak, nonatomic) id<MuseumCellDelegate>delegate;
@property (assign, nonatomic) NSInteger cellIndex;

#pragma mark get Painting object from MusemVC.
- (void)setDataWithPaintingObject:(MSPainting*)objPainting;


@end
