//
//  MSHelper.h
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MSHelper : NSObject

+ (MSHelper*)sharedInstance;
+ (BOOL)isBlank:(NSString*)string;
- (void)showAlertViewWithMessage:(NSString*)message atViewController:(id)viewController;
- (UIColor*)getUIColorObjectFromHexString:(NSString *)hexStr;
- (float)calculateTextViewHeightWithText:(NSString*)textViewText withFontSize:(NSInteger)fontSize andView:(UIView*)view;
- (void)hideProgressFromView:(UIView*)view;
- (void)showProgressToView:(UIView*)view;
@end

