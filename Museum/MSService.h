//
//  MSService.h
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSService : NSObject

+ (MSService*) sharedInstance;
- (NSString*)getBaseURL;

- (void)getPaintingsWithBlock:(void (^)(bool success, id responseObject))block;
@end
