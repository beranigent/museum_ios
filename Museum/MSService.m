//
//  MSService.m
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSService.h"
#import "AFNetworking.h"
#import "MSConstants.h"


@implementation MSService

#pragma mark MSService Singleton
+ (MSService*) sharedInstance{
    static dispatch_once_t _singletonPredicate;
    static MSService *_singleton = nil;
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[super allocWithZone:nil] init];
    });
    return _singleton;
}

#pragma mark Initialie AFNetwork Manager
-(AFHTTPSessionManager*)getAFManager{
    AFHTTPSessionManager *afSesionManager = [AFHTTPSessionManager manager];
    afSesionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    afSesionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [afSesionManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    afSesionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    return afSesionManager;
}

#pragma mark Method that return Base Endpoint Address from Constant class
- (NSString*)getBaseURL{
    return CONST_BASE_URL;
}

#pragma mark get paintings data from server
- (void)getPaintingsWithBlock:(void (^)(bool success, id responseObject))block
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    NSString*urlString = [NSString stringWithFormat:@"%@paintings.json",[self getBaseURL]];
    
    [[self getAFManager] GET:urlString parameters:@{} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        block(YES, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        if(task.response){
            block(NO, task.response);
        }else{
            block(NO, CONST_MESSAGE_WRONG);
        }
    }];
}
@end
