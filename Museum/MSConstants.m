//
//  MSConstants.m
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSConstants.h"

@implementation MSConstants

NSString *const CONST_BASE_URL                          = @"https://www.bevuta.com/museum/";
NSString *const CONST_COLLECTION_VIEW_CELL_IDENTIFIER   = @"Cell";
NSString *const CONST_APP_NAME                          = @"Museum";
NSString *const CONST_VERDANA_FONT_NAME                 = @"Verdana";
NSString *const CONST_MESSAGE_WRONG                     = @"Something went wrong, please try again!";

@end
