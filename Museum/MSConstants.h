//
//  MSConstants.h
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MSConstants : NSObject

FOUNDATION_EXPORT  NSString *const CONST_BASE_URL;
FOUNDATION_EXPORT  NSString *const CONST_COLLECTION_VIEW_CELL_IDENTIFIER;
FOUNDATION_EXPORT  NSString *const CONST_APP_NAME;
FOUNDATION_EXPORT  NSString *const CONST_VERDANA_FONT_NAME;
FOUNDATION_EXPORT  NSString *const CONST_MESSAGE_WRONG;

@end
