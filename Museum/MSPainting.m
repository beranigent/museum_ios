//
//  MSPainting.m
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSPainting.h"

@implementation MSPainting
- (instancetype)initWithData:(id)data{
    self = [super init];
    if(self){
        self.image_url = [NSURL URLWithString:[data objectForKey:@"image_url"]];
        self.info_text = [data objectForKey:@"info_text"];
        self.info_text_color = [data objectForKey:@"info_text_color"];
        
        if([data objectForKey:@"info_text_size"]){
            self.info_text_size = [[data objectForKey:@"info_text_size"] integerValue];
        }else{
            self.info_text_size = 16;//default text size is 16.
        }
    }
    return self;
}
@end
