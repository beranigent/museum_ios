//
//  MSMuseumViewCell.m
//  Museum
//
//  Created by Gent Berani on 7/23/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSMuseumViewCell.h"
#import "MSConstants.h"
#import "MSHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation MSMuseumViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setDataWithPaintingObject:(MSPainting*)objPainting{
    UIImageView* paintingImage = (UIImageView*)[self viewWithTag:1];
    UITextView* paintingText   = (UITextView*)[self viewWithTag:2];
    [self addTapGestureToUITextView:paintingText];
    paintingText.font = [UIFont fontWithName:CONST_VERDANA_FONT_NAME size:[objPainting getInfoTextSize]];
    paintingText.text = objPainting.getInfoText;
    [self setTextColorToUITextView:paintingText withPaintingObject:objPainting];
    [self loadImageToImageView:paintingImage andPaintingObject:objPainting];
}

- (void)loadImageToImageView:(UIImageView*)imageView andPaintingObject:(MSPainting*)objPainting{
    [imageView sd_setImageWithURL:[objPainting getImageURL]
                 placeholderImage:nil
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            if (image) {
                                imageView.contentMode = UIViewContentModeScaleAspectFit;
                                imageView.image = image;
                            }else{
                                imageView.contentMode = UIViewContentModeCenter;
                                imageView.image = [UIImage imageNamed:@"nopic"];
                            }
                        }];
}

//set textcolor to textview if UITextView
- (void)setTextColorToUITextView:(UITextView*)textView withPaintingObject:(MSPainting*)objPainting{
    if([objPainting isTextViewClicked]){
        textView.textColor = [[MSHelper sharedInstance] getUIColorObjectFromHexString:[objPainting getInfoTextColor]];
    }else{
        textView.textColor = [UIColor blackColor];
    }
}
//adding tap gesture to Painting UITextView
- (void)addTapGestureToUITextView:(UITextView*)textView{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textViewClicked:)];
    [textView addGestureRecognizer:tap];
    [textView setUserInteractionEnabled:YES];
}

//on Painting Text Click
- (void)textViewClicked:(UITapGestureRecognizer *)tapGesture{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didUserClickOnTextViewAtIndex:)]) {
        [self.delegate didUserClickOnTextViewAtIndex:_cellIndex];
    }
}

@end

