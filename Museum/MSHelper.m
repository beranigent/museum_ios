//
//  MSHelper.m
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSHelper.h"
#import "MSMuseumViewController.h"
#import "MSConstants.h"
#import "MBProgressHUD.h"

@implementation MSHelper

#pragma MSHelper mark Singleton
+ (MSHelper*) sharedInstance{
    static dispatch_once_t _singletonPredicate;
    static MSHelper *_singleton = nil;
    dispatch_once(&_singletonPredicate, ^{
        _singleton = [[super allocWithZone:nil] init];
    });
    return _singleton;
}

#pragma mark method that check if input string is blank
+ (BOOL)isBlank:(NSString*)string{
    if([string isKindOfClass:[NSString class]]){
        if(string.length>0 && ![string isEqualToString:@"null"]){
            return NO;
        }
        return YES;
    }
    return YES;
}

#pragma mark Alert View with custom message
- (void)showAlertViewWithMessage:(NSString*)message atViewController:(id)viewController{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:CONST_APP_NAME
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Retry"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         if([viewController isKindOfClass:[MSMuseumViewController class]]){
                                                             MSMuseumViewController*museumVC = (MSMuseumViewController*)viewController;
                                                             [museumVC requestForPaintingData];
                                                         }
                                                     }];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:actionCancel];
    [alertController addAction:actionOk];
    [viewController presentViewController:alertController animated:YES completion:nil];
}


#pragma get UIColor from HEX
- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr{
    CGFloat alpha = 1.0;
    unsigned int hexint = [self intFromHexString:hexStr];
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    return color;
}
- (unsigned int)intFromHexString:(NSString *)hexStr{
    unsigned int hexInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexInt];
    return hexInt;
}

#pragma mark this method calculate height of UITextView
- (float)calculateTextViewHeightWithText:(NSString*)textViewText withFontSize:(NSInteger)fontSize andView:(UIView*)view{
    float textViewHeight = 0;
    
    CGRect textViewRect = [textViewText boundingRectWithSize:CGSizeMake(view.frame.size.width - 10, FLT_MAX)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{ NSFontAttributeName : [UIFont fontWithName:CONST_VERDANA_FONT_NAME size:fontSize] }
                                                     context:nil];
    textViewHeight = textViewRect.size.height;
    return textViewHeight;
}
#pragma mark show/hide MBProgressHUD
- (void)showProgressToView:(UIView*)view{
    [MBProgressHUD showHUDAddedTo:view animated:YES];
}
- (void)hideProgressFromView:(UIView*)view{ 
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:view animated:YES];
        });
    });
}


@end
