//
//  NSMutableArray+MSArray.m
//  Museum
//
//  Created by Gent Berani on 7/23/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSArray.h"
#import "MSPainting.h"
@implementation NSMutableArray (MSArray)

-(instancetype)initPaintingArrayWithData:(id)data{
    self = [self init];
    if(self){
        NSArray*paintingArray = [data objectForKey:@"paintings"];
        if(paintingArray){
            for(NSDictionary*object in paintingArray){
                [self addObject:[[MSPainting alloc] initWithData:object]];
            }
        }
    }
    return self;
}

@end
