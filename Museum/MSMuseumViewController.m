//
//  ViewController.m
//  Museum
//
//  Created by Gent Berani on 7/22/16.
//  Copyright © 2016 berani. All rights reserved.
//

#import "MSMuseumViewController.h"
#import "MSConstants.h"
#import "MBProgressHUD.h"
#import "MSHelper.h"
#import "MSService.h"
#import "MSArray.h"
#import "MSPainting.h"

@interface MSMuseumViewController ()

@end

@implementation MSMuseumViewController

#pragma mark lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpCollectionView];
    self.paintingArray = [[NSMutableArray alloc] init];
    [self requestForPaintingData];
    
}

#pragma mark Museum Class methods
- (void)setUpCollectionView{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    UINib*museumCellNib = [UINib nibWithNibName:@"MSMuseumViewCell" bundle:nil];
    [self.collectionView registerNib:museumCellNib forCellWithReuseIdentifier:CONST_COLLECTION_VIEW_CELL_IDENTIFIER];
    
}

- (void)requestForPaintingData{
    [self.noResultView setHidden:YES];
    [[MSHelper sharedInstance] showProgressToView:self.view];
    [[MSService sharedInstance] getPaintingsWithBlock:^(bool success, id responseObject) {
        if(success){
            self.paintingArray = [[NSMutableArray alloc] initPaintingArrayWithData:responseObject];
            [self.collectionView reloadData];
            [self.noResultView setHidden:YES];
        }else{
            [self.noResultView setHidden:NO];
            [[MSHelper sharedInstance] showAlertViewWithMessage:responseObject atViewController:self];
        }
        [[MSHelper sharedInstance] hideProgressFromView:self.view];
    }];
}

#pragma mark CollectionView DataSources and Delegates
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.paintingArray count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MSMuseumViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CONST_COLLECTION_VIEW_CELL_IDENTIFIER forIndexPath:indexPath];
    cell.delegate = self;
    cell.cellIndex = indexPath.row;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    MSPainting*objPainting = (MSPainting*)[self.paintingArray objectAtIndex:indexPath.row];
    [(MSMuseumViewCell*)cell setDataWithPaintingObject:objPainting];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    MSPainting*objPainting = (MSPainting*)[self.paintingArray objectAtIndex:indexPath.row];
    
    CGSize collectionViewCellSize = collectionView.frame.size;
    CGFloat paintingImageViewHeight = 0.5 * collectionViewCellSize.width;
    CGFloat spaceBetweenCells = 7.5;
    CGFloat paintingTextViewHeight = [[MSHelper sharedInstance] calculateTextViewHeightWithText:[objPainting getInfoText] withFontSize:[objPainting getInfoTextSize] andView:self.collectionView];
    
    
    collectionViewCellSize.height = paintingImageViewHeight + paintingTextViewHeight + spaceBetweenCells;
    return collectionViewCellSize;
}

#pragma mark On Painting TextView click
- (void)didUserClickOnTextViewAtIndex:(NSInteger)cellIndex{
    MSPainting*objPainting = (MSPainting*)[self.paintingArray objectAtIndex:cellIndex];
    objPainting.textViewClicked = !objPainting.textViewClicked;
    [self.paintingArray replaceObjectAtIndex:cellIndex withObject:objPainting];
    [self.collectionView reloadData];
}
#pragma mark tap to retry action
- (IBAction)retryAction:(UIButton *)sender {
    [self requestForPaintingData];
}
@end
